cmake_minimum_required(VERSION 3.10.2)
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_LIST_DIR}/CMakeModules)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMSimoxTools)

set(PROJ_VERSION 0.0.2)
set(PROJ_SO_VERSION 1) # shared lib (.so file) build number

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMSimoxTools.cpp
    RobotPoseDifferentialIK.cpp
)

set(HEADER_FILES
    MMMSimoxTools.h
    RobotPoseDifferentialIK.h
    MMMSimoxToolsImportExport.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)

###########################################################
#### Project build configuration                       ####
###########################################################

add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_include_directories(${PROJECT_NAME} PUBLIC ${EXTERNAL_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl)

# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/..> # TODO: other code requires headers like "#include <MMMSimoxTools/xyz.h>" so we need
    $<INSTALL_INTERFACE:include>                      # TODO: the parent dir for BUILD_INTERFACE. find a more elegant way i guess?
)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    #RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include # seems unnecessary because of target_include_directories()
    COMPONENT lib
)

install(FILES ${HEADER_FILES} DESTINATION "include/${PROJECT_NAME}" COMPONENT dev)

###########################################################
#### Version configuration                             ####
###########################################################

set_property(TARGET ${PROJECT_NAME} PROPERTY VERSION ${PROJ_VERSION})
set_property(TARGET ${PROJECT_NAME} PROPERTY SOVERSION ${PROJ_SO_VERSION})

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
    VERSION ${PROJ_VERSION}
    COMPATIBILITY AnyNewerVersion
)

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

set(CMAKE_POSITION_INDEPENDENT_CODE ON) # enable -fPIC
