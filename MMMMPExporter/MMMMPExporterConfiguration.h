#ifndef __MMM_MPEXPORTERCONFIGURATION_H_
#define __MMM_MPEXPORTERCONFIGURATION_H_

#include <filesystem>
#include <string>

#include <MMM/MMMCore.h>
#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MPExporter.
*/
class MMMMPExporterConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string inputMotionPath;
    std::string inputMotionDir;
    std::string nodeSetName;
    std::string outputDir;
    std::string mpType;
    int kernelSize;
    int numSamples;

    MMMMPExporterConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotion", "mmm motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("inputMotionDir", "mmm motion file directory");
        VirtualRobot::RuntimeEnvironment::considerKey("kernelSize", "kernel size of MP");
        VirtualRobot::RuntimeEnvironment::considerKey("numSamples", "number of samples");
        VirtualRobot::RuntimeEnvironment::considerKey("NodeSetName", "NodeSet to be considered");
        VirtualRobot::RuntimeEnvironment::considerKey("MPType", "MP Type");
        VirtualRobot::RuntimeEnvironment::considerKey("outputDir");
        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        inputMotionPath = getParameter("inputMotion", false, true);
        inputMotionDir = getParameter("inputMotionDir", false, false);
        if (inputMotionPath.empty() && inputMotionDir.empty())
            valid = false;

        outputDir = getParameter("outputDir", false, false);
        if (outputDir.empty())
            outputDir = inputMotionPath.empty() ? inputMotionDir : std::filesystem::path(inputMotionPath).parent_path().string();

        kernelSize = VirtualRobot::RuntimeEnvironment::getValue<int>("kernelSize", 100);
        numSamples = VirtualRobot::RuntimeEnvironment::getValue<int>("numSamples", 1000);

        mpType = getParameter("MPType", false, false);
        if (mpType.empty())
            mpType = "UMITSMP";

        nodeSetName = getParameter("NodeSetName", false, false);
        if (nodeSetName.empty())
            nodeSetName = "RightArm";

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MPExporter Configuration ***" << std::endl;
        std::cout << "Input motion: " << inputMotionPath << std::endl;
        std::cout << "Input motion directory: " << inputMotionDir << std::endl;
        std::cout << "Output motion: " << outputDir << std::endl;
    }
};


#endif
