#ifndef __MMM_MPEXPORTERPLUGIN_H_
#define __MMM_MPEXPORTERPLUGIN_H_

#include "../../MMMViewer/MotionHandler.h"

namespace MMM
{

class MPExporterPlugin : public MotionHandler
{
    Q_OBJECT

public:
    MPExporterPlugin(QWidget* widget);

    void handleMotion(MotionList motions);

    virtual std::string getName();

    static constexpr const char* NAME = "MPExporter";

private:
    std::string searchPath;
    QWidget* widget;
};

}

#endif
