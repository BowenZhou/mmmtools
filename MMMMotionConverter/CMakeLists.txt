cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(MMMMotionConverter)

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    MMMConverter.cpp
    MMMConverterConfiguration.cpp
    ../common/ApplicationBaseConfiguration.cpp
)

set(HEADER_FILES
    MMMConverterConfiguration.h
    MotionConverter.h
    MotionConverterFactory.h
    ../common/ApplicationBaseConfiguration.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)

set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMSimoxTools)

###########################################################
#### Project build configuration                       ####
###########################################################

add_executable(${PROJECT_NAME} ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} ${EXTERNAL_LIBRARY_DIRS} dl)

set(MMMConverter_BASE_DIR ${PROJECT_SOURCE_DIR} CACHE INTERNAL "")
add_definitions(-DMMMConverter_BASE_DIR="${MMMConverter_BASE_DIR}" -D_SCL_SECURE_NO_WARNINGS)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    #LIBRARY DESTINATION lib
    #ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include
    COMPONENT bin
)

add_definitions(-DMMMTools_LIB_DIR="${MMMTools_LIB_DIR}")
add_definitions(-DMMMTools_SRC_DIR="${MMMTools_SOURCE_DIR}")
add_definitions(-DMOTION_CONVERTER_PLUGIN_LIB_DIR="${MOTION_CONVERTER_PLUGIN_LIB_DIR}")

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})

###########################################################
#### Motion Converter Plugins                          ####
###########################################################

# Add motion converter
function(DefineMotionConverterPlugin MotionConverterPlugin_Name MotionConverterPlugin_Sources MotionConverterPlugin_Headers)
    add_library(${MotionConverterPlugin_Name} SHARED ${MotionConverterPlugin_Sources} ${MotionConverterPlugin_Headers})
    target_link_libraries(${MotionConverterPlugin_Name} PUBLIC ${EXTERNAL_LIBRARY_DIRS} MoCapMarkerSensor ModelPoseSensor KinematicSensor)
    set_target_properties(${MotionConverterPlugin_Name} PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${MOTION_CONVERTER_PLUGIN_LIB_DIR})

    install(
        TARGETS ${MotionConverterPlugin_Name}
        EXPORT "${CMAKE_PROJECT_NAME}Targets"
        LIBRARY DESTINATION lib/mmm_plugins/motion_converter
        #ARCHIVE DESTINATION lib
        #RUNTIME DESTINATION bin
        #INCLUDES DESTINATION include
        COMPONENT lib
    )
    install(FILES ${MotionConverterPlugin_Headers} DESTINATION "include/MMM/${MotionConverterPlugin_Name}" COMPONENT dev)
endfunction()

add_subdirectory(MotionConverterPlugin)

###########################################################
#### Motion Handler Plugin                             ####
###########################################################

add_subdirectory(MotionHandlerPlugin)
