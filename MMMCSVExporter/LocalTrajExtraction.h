/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Franziska Krebs
* @copyright  2020 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_LocalTrajExtraction_H_
#define __MMM_LocalTrajExtraction_H_

#include "../MMMViewer/MotionHandler.h"
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
namespace MMM
{

class LocalTrajExtraction;
typedef boost::shared_ptr<LocalTrajExtraction> LocalTrajExtractionPtr;
class LocalTrajExtraction //: public MotionHandler
{
    //Q_OBJECT

public:
    LocalTrajExtraction(MMM::MotionList motions_in, float minTimestep_in, float maxTimestep_in, int framesPerSecond_in, std::string modelNameA_in, std::string modelNameB_in);

    MotionPtr getLocalPoseTraj();

private:
    MMM::MotionList motions;
    float minTimestep;
    float maxTimestep;
    int framesPerSecond;
    std::string modelNameA;
    std::string modelNameB;

    Eigen::Matrix4f returnTrafo (boost::shared_ptr<MMM::ModelPoseSensor> poseSensor, float t);
    //std::string searchPath;
    //QWidget* widget;
};

}

#endif
