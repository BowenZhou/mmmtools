#include "SynchronizeMotionHandler.h"
#include "SynchronizeMotionHandlerDialog.h"

using namespace MMM;

SynchronizeMotionHandler::SynchronizeMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Synchronize"),
    widget(widget)
{
}

void SynchronizeMotionHandler::handleMotion(MotionList motions) {
    if (motions.size() > 0) {
        SynchronizeMotionHandlerDialog* dialog = new SynchronizeMotionHandlerDialog(widget, motions);
        if (dialog->synchronizeMotion()) emit openMotions(motions);
    }
    else MMM_ERROR << "Cannot open synchronize motion handler dialog, because no motions are present!" << std::endl;
}

std::string SynchronizeMotionHandler::getName() {
    return NAME;
}
