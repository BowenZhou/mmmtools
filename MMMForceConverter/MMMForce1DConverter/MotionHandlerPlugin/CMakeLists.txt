project(AddForce1DHandler)

set(AddForce1DHandler_Sources
    AddForce1DHandlerFactory.cpp
    AddForce1DHandler.cpp
    ../AddForce1DHandlerSensorConfiguration.cpp
    ../../../common/AddAttachedSensor/AddAttachedSensorDialog.cpp
)

set(AddForce1DHandler_Headers
    AddForce1DHandlerFactory.h
    AddForce1DHandler.h
    ../AddForce1DHandlerSensorConfiguration.h
    ../../../common/AddAttachedSensor/AddAttachedSensorDialog.h
)

set(AddForce1DHandler_Moc
    AddForce1DHandler.h
    ../../../MMMViewer/MotionHandler.h
    ../../../common/AddAttachedSensor/AddAttachedSensorDialog.h
)

set(AddForce1DHandler_Ui
    ../../../common/AddAttachedSensor/AddAttachedSensorDialog.ui
)

set(AddForce1DHandler_Sensors Force1DSensor)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${AddForce1DHandler_Sources}" "${AddForce1DHandler_Headers}" "${AddForce1DHandler_Moc}" "${AddForce1DHandler_Ui}" "${AddForce1DHandler_Sensors}")
