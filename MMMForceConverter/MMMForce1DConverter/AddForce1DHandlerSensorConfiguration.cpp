#include "AddForce1DHandlerSensorConfiguration.h"

using namespace MMM;


AddForce1DHandlerSensorConfiguration::AddForce1DHandlerSensorConfiguration() : AddAttachedSensorConfiguration<Force1DSensor>("force1D", {"Force"})
{
}

void AddForce1DHandlerSensorConfiguration::addSensorMeasurements(const std::vector<std::map<std::string, std::string> > &values) {
    for (std::map<std::string, std::string> map : values) {
        float timestep = XML::convertTo<float>(map[TIMESTEP_STR], "Timestep is no valid float!");
        float force = XML::convertTo<float>(map["Force"], "Force is no valid float!");
        Force1DSensorMeasurementPtr measurement(new Force1DSensorMeasurement(timestep, force));
        sensor->addSensorMeasurement(measurement);
    }
}

