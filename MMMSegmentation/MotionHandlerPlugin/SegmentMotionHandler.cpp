#include "SegmentMotionHandler.h"
#include <QFileDialog>

using namespace MMM;

SegmentMotionHandler::SegmentMotionHandler(QWidget* widget) :
    MotionHandler(MotionHandlerType::GENERAL, "Segment"),
    dialog(new SegmentMotionHandlerDialog(widget))
{
    connect(dialog, SIGNAL(jumpTo(float)), this, SIGNAL(jumpTo(float)));
}

void SegmentMotionHandler::handleMotion(MotionList motions) {
    if (motions.size() > 0) dialog->segmentMotion(motions);
    else MMM_ERROR << "Cannot open segment motion dialog, because no motions are present!" << std::endl;
}

std::string SegmentMotionHandler::getName() {
    return NAME;
}

boost::shared_ptr<IPluginHandler> SegmentMotionHandler::getPluginHandler() {
    boost::shared_ptr<PluginHandler<MMM::MotionSegmenterFactory> > pluginHandler(new PluginHandler<MMM::MotionSegmenterFactory>(getPluginHandlerID(), MOTION_SEGMENTER_PLUGIN_LIB_DIR));
    pluginHandler->updateSignal.connect(boost::bind(&SegmentMotionHandlerDialog::update, dialog, _1));
    pluginHandler->emitUpdate();

    return pluginHandler;
}

std::string SegmentMotionHandler::getPluginHandlerID() {
    return "Motion Segmenter";
}

