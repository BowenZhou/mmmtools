#include "AddIMUHandlerFactory.h"
#include "AddIMUHandler.h"

#include <boost/extension/extension.hpp>

using namespace MMM;

// register this factory
MotionHandlerFactory::SubClassRegistry AddIMUHandlerFactory::registry(AddIMUHandler::NAME, &AddIMUHandlerFactory::createInstance);

AddIMUHandlerFactory::AddIMUHandlerFactory() : MotionHandlerFactory() {}

AddIMUHandlerFactory::~AddIMUHandlerFactory() {}

std::string AddIMUHandlerFactory::getName() {
    return AddIMUHandler::NAME;
}

MotionHandlerPtr AddIMUHandlerFactory::createMotionHandler(QWidget* parent) {
    return MotionHandlerPtr(new AddIMUHandler(parent));
}

MotionHandlerFactoryPtr AddIMUHandlerFactory::createInstance(void *) {
    return MotionHandlerFactoryPtr(new AddIMUHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL MotionHandlerFactoryPtr getFactory() {
    return MotionHandlerFactoryPtr(new AddIMUHandlerFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return MotionHandlerFactory::VERSION;
}
