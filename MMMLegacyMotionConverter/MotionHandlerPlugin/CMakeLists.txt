project(LegacyMotionImporter)

set(LegacyMotionImporter_Sources
    LegacyMotionImporterFactory.cpp
    LegacyMotionImporter.cpp
    LegacyMotionImporterDialog.cpp
    ../LegacyMotionConverter.cpp
    ../../common/MotionListWidget.cpp
    ../../common/MotionListConfiguration.cpp
    ../../common/HandleMotionsWithoutModel.cpp
)

set(LegacyMotionImporter_Headers
    LegacyMotionImporterFactory.h
    LegacyMotionImporter.h
    LegacyMotionImporterDialog.h
    ../LegacyMotionConverter.h
    ../../common/MotionListWidget.h
    ../../common/MotionListConfiguration.h
    ../../common/HandleMotionsWithoutModel.h
)

set(LegacyMotionImporter_Moc
    LegacyMotionImporterDialog.h
    LegacyMotionImporter.h
    ../../common/MotionListWidget.h
    ../../MMMViewer/MotionHandler.h
)

set(LegacyMotionImporter_Ui
    LegacyMotionImporterDialog.ui
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${LegacyMotionImporter_Sources}" "${LegacyMotionImporter_Headers}" "${LegacyMotionImporter_Moc}" "${LegacyMotionImporter_Ui}" "")
