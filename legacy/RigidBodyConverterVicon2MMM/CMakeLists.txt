cmake_minimum_required(VERSION 3.10.2)

###########################################################
#### Project configuration                             ####
###########################################################

project(RigidBodyConverterVicon2MMM)

###########################################################
#### Source configuration                              ####
###########################################################

set(SOURCE_FILES
    ${PROJECT_SOURCE_DIR}/RigidBodyConverter.cpp
    ${PROJECT_SOURCE_DIR}/RigidBodyConverterFactory.cpp
)

set(HEADER_FILES
    #${PROJECT_SOURCE_DIR}/RigidBodyConverterVicon2MMM.h
    ${PROJECT_SOURCE_DIR}/RigidBodyConverter.h
    ${PROJECT_SOURCE_DIR}/RigidBodyConverterFactory.h
    #${PROJECT_SOURCE_DIR}/RigidBodyConverterImportExport.h
    ${PROJECT_SOURCE_DIR}/RigidBodyConverterVicon2MMMImportExport.h
)

###########################################################
#### CMake package configuration                       ####
###########################################################

find_package(MMMCore REQUIRED)
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMCore)

find_package(Simox REQUIRED)
set(EXTERNAL_INCLUDE_DIRS ${EXTERNAL_INCLUDE_DIRS} "${PROJECT_SOURCE_DIR}/../3rdParty") # boost extension
set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} VirtualRobot)

set(EXTERNAL_LIBRARY_DIRS ${EXTERNAL_LIBRARY_DIRS} MMMSimoxTools)

###########################################################
#### Project build configuration                       ####
###########################################################

add_library(${PROJECT_NAME} SHARED ${SOURCE_FILES} ${HEADER_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ${EXTERNAL_LIBRARY_DIRS} dl ConverterVicon2MMM)

# this allows dependant targets to automatically add include-dirs by simply linking against this project
target_include_directories(${PROJECT_NAME} PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>
)

install(
    TARGETS ${PROJECT_NAME}
    EXPORT "${CMAKE_PROJECT_NAME}Targets"
    LIBRARY DESTINATION lib
    ARCHIVE DESTINATION lib
    RUNTIME DESTINATION bin
    #INCLUDES DESTINATION include # seems unnecessary because of target_include_directories()
    COMPONENT dev
)

install(FILES ${HEADER_FILES} DESTINATION "include/${PROJECT_NAME}" COMPONENT dev)

###########################################################
#### Compiler configuration                            ####
###########################################################

include_directories(${EXTERNAL_INCLUDE_DIRS})
link_directories(${EXTERNAL_LIBRARY_DIRS})
add_definitions(${EXTERNAL_LIBRARY_FLAGS})