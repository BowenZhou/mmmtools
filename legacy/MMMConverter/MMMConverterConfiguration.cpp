/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMMTools
* @copyright  2015 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ConverterApplicationHelper.h"
#include "MMMConverterConfiguration.h"

bool MMMConverterConfiguration::processCommandLine(int argc, char *argv[])
{
    VirtualRobot::RuntimeEnvironment::considerKey("outputFile");
    VirtualRobot::RuntimeEnvironment::considerKey("outputMotionName");

    ConverterApplicationBaseConfiguration::processCommandLine(argc, argv);

    outputFile = getParameter("outputFile", false, false);
    if (outputFile.empty())
        outputFile = "MMMConverter_output.xml";

    outputMotionName = getParameter("outputMotionName", false, false);

    return valid;
}

void MMMConverterConfiguration::print()
{
    ConverterApplicationBaseConfiguration::print();

    std::cout << "Output file: " << outputFile << std::endl;
    std::cout << "Output motion name: " << outputMotionName << std::endl;
}
