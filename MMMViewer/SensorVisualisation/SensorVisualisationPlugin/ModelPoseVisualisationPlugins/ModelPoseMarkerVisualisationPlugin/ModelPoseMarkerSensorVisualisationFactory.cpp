#include "ModelPoseMarkerSensorVisualisationFactory.h"
#include "ModelPoseMarkerSensorVisualisation.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>

using namespace MMM;

// register this factory
SensorVisualisationFactory::SubClassRegistry ModelPoseMarkerSensorVisualisationFactory::registry(VIS_STR(ModelPoseSensor::TYPE), &ModelPoseMarkerSensorVisualisationFactory::createInstance);

ModelPoseMarkerSensorVisualisationFactory::ModelPoseMarkerSensorVisualisationFactory() : SensorVisualisationFactory() {}

ModelPoseMarkerSensorVisualisationFactory::~ModelPoseMarkerSensorVisualisationFactory() {}

std::string ModelPoseMarkerSensorVisualisationFactory::getName()
{
    return VIS_STR(ModelPoseSensor::TYPE);
}

SensorVisualisationPtr ModelPoseMarkerSensorVisualisationFactory::createSensorVisualisation(SensorPtr sensor, VirtualRobot::RobotPtr robot, VirtualRobot::CoinVisualizationPtr visualization, SoSeparator* sceneSep) {
    ModelPoseSensorPtr s = boost::dynamic_pointer_cast<ModelPoseSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << ModelPoseSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorVisualisationPtr(new ModelPoseMarkerSensorVisualisation(s, robot, visualization, sceneSep));
}

SensorVisualisationFactoryPtr ModelPoseMarkerSensorVisualisationFactory::createInstance(void *)
{
    return SensorVisualisationFactoryPtr(new ModelPoseMarkerSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorVisualisationFactoryPtr getFactory() {
    return SensorVisualisationFactoryPtr(new ModelPoseMarkerSensorVisualisationFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorVisualisationFactory::VERSION;
}
