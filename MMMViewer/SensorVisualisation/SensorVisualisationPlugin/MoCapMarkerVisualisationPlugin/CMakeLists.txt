project(MoCapMarkerSensorVisualisation)

set(MoCapMarkerSensorVisualisation_Sources
    MoCapMarkerSensorVisualisationFactory.cpp
    MoCapMarkerSensorVisualisation.cpp
)

set(MoCapMarkerSensorVisualisation_Headers
    MoCapMarkerSensorVisualisationFactory.h
    MoCapMarkerSensorVisualisation.h
)

DefineMotionSensorVisualisationPlugin(${PROJECT_NAME} "${MoCapMarkerSensorVisualisation_Sources}" "${MoCapMarkerSensorVisualisation_Headers}" MoCapMarkerSensor)
