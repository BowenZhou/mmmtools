#include "KinematicSensorPlotFactory.h"
#include "KinematicSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry KinematicSensorPlotFactory::registry(PLT_STR(KinematicSensor::TYPE), &KinematicSensorPlotFactory::createInstance);

KinematicSensorPlotFactory::KinematicSensorPlotFactory() : SensorPlotFactory() {}

KinematicSensorPlotFactory::~KinematicSensorPlotFactory() {}

std::string KinematicSensorPlotFactory::getName()
{
    return PLT_STR(KinematicSensor::TYPE);
}

std::string KinematicSensorPlotFactory::getType()
{
    return KinematicSensor::TYPE;
}

std::string KinematicSensorPlotFactory::getUnit() {
    return "Joint angle (radian)";
}

std::string KinematicSensorPlotFactory::getValueName() {
    return "Kinematic joint";
}

SensorPlotPtr KinematicSensorPlotFactory::createSensorPlot(SensorPtr sensor) {
    KinematicSensorPtr s = boost::dynamic_pointer_cast<KinematicSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << KinematicSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorPlotPtr(new KinematicSensorPlot(s));
}

SensorPlotFactoryPtr KinematicSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new KinematicSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new KinematicSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
