#include "MoCapMarkerSensorPlotFactory.h"
#include "MoCapMarkerSensorPlot.h"

#include <boost/extension/extension.hpp>

#include <MMM/Motion/Plugin/MoCapMarkerPlugin/MoCapMarkerSensor.h>

using namespace MMM;

// register this factory
SensorPlotFactory::SubClassRegistry MoCapMarkerSensorPlotFactory::registry(PLT_STR(MoCapMarkerSensor::TYPE), &MoCapMarkerSensorPlotFactory::createInstance);

MoCapMarkerSensorPlotFactory::MoCapMarkerSensorPlotFactory() : SensorPlotFactory() {}

MoCapMarkerSensorPlotFactory::~MoCapMarkerSensorPlotFactory() {}

std::string MoCapMarkerSensorPlotFactory::getName()
{
    return PLT_STR(MoCapMarkerSensor::TYPE);
}

std::string MoCapMarkerSensorPlotFactory::getType()
{
    return MoCapMarkerSensor::TYPE;
}

std::string MoCapMarkerSensorPlotFactory::getUnit() {
    return "Motion capture marker position (in cm)";
}

std::string MoCapMarkerSensorPlotFactory::getValueName() {
    return "Motion Capture Marker";
}

SensorPlotPtr MoCapMarkerSensorPlotFactory::createSensorPlot(SensorPtr sensor) {
    MoCapMarkerSensorPtr s = boost::dynamic_pointer_cast<MoCapMarkerSensor>(sensor);
    if (!s) {
        MMM_ERROR << sensor->getType() << " could not be castet to " << MoCapMarkerSensor::TYPE << std::endl;
        return nullptr;
    }
    return SensorPlotPtr(new MoCapMarkerSensorPlot(s));
}

SensorPlotFactoryPtr MoCapMarkerSensorPlotFactory::createInstance(void *)
{
    return SensorPlotFactoryPtr(new MoCapMarkerSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL SensorPlotFactoryPtr getFactory() {
    return SensorPlotFactoryPtr(new MoCapMarkerSensorPlotFactory());
}

extern "C"
BOOST_EXTENSION_EXPORT_DECL std::string getVersion() {
    return SensorPlotFactory::VERSION;
}
