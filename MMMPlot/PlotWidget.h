/*
This file is part of MMM.

MMM is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MMM is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with MMM.  If not, see <http://www.gnu.org/licenses/>.
*
* @package    MMM
* @author     Andre Meixner
* @copyright  2018 High Performance Humanoid Technologies (H2T), Karlsruhe, Germany
*
*/

#ifndef __MMM_PLOTWIDGET_H_
#define __MMM_PLOTWIDGET_H_

#include <QWidget>
#include <set>

#ifndef Q_MOC_RUN
#include "SensorPlotFactory.h"
#include "SinglePlotWidget.h"
#include <MMM/FactoryPluginLoader.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <QPushButton>
#endif

namespace Ui {
class PlotWidget;
}

class PlotWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlotWidget(QWidget *parent = 0, const std::vector<std::string> &sensorPluginPaths = std::vector<std::string>(), bool standalone = false);
    ~PlotWidget();
    void loadMotions(const std::vector<std::string> &motionFilePaths);
    void loadMotions(MMM::MotionList motions);
    void addImportButton(QPushButton* button);
    void update(std::map<std::string, MMM::SensorPlotFactoryPtr> sensorPlotFactories);
    void moveSlider(float timestep);
    boost::shared_ptr<MMM::FactoryPluginLoader<MMM::SensorPlotFactory> > getFactoryLoader();

private slots:
    void plotTypeChanged(QString type);
    void loadAdditionalMotions();
    void newPlot();
    void plotTabChanged();

signals:
    void showMessage(const std::string &message);
    void openMotions(MMM::MotionList motions);
    void jumpTo(float timestep);

private:
    Ui::PlotWidget *ui;
    std::map<std::string, boost::shared_ptr<MMM::SensorPlotFactory> > factories;
    boost::shared_ptr<MMM::FactoryPluginLoader<MMM::SensorPlotFactory> > factoryPluginLoader;
    std::set<std::string> plotNames;
    std::map<std::string, MMM::MotionList> motions;
    std::vector<SinglePlotWidget*> plotWidgets;
    SinglePlotWidget* currentPlotWidget;
    bool standalone;
    float currentTimestep;
    MMM::MotionReaderXMLPtr motionReader;
};

#endif // PLOTWIDGET_H
