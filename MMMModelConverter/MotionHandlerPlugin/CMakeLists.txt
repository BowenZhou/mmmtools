project(ModelImporter)

set(ModelImporter_Sources
    ModelImporterFactory.cpp
    ModelImporter.cpp
    ModelImporterDialog.cpp
)

set(ModelImporter_Headers
    ModelImporterFactory.h
    ModelImporter.h
    ModelImporterDialog.h
)

set(ModelImporter_Moc
    ModelImporterDialog.h
    ../../MMMViewer/MotionHandler.h
    ModelImporter.h
)

set(ModelImporter_Ui
    ModelImporterDialog.ui
)

set(Sensors
    ModelPoseSensor
    KinematicSensor
)

DefineMotionHandlerPlugin(${PROJECT_NAME} "${ModelImporter_Sources}" "${ModelImporter_Headers}" "${ModelImporter_Moc}" "${ModelImporter_Ui}" "${Sensors}")
