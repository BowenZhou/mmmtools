#ifndef __MMM_MPLEARNER_H_
#define __MMM_MPLEARNER_H_

#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/Motion/MotionWriterXML.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <dmp/representation/dmp/umidmp.h>
#include <dmp/representation/dmp/umitsmp.h>
#include <dmp/representation/trajectory.h>

#include <boost/filesystem.hpp>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <dmp/representation/dmpfactory.h>

namespace MMM {
class MPLearner
{
public:
    MPLearner()
    {
    }

    DMP::MPConfigPtr learnMP(DMP::DMPInterfacePtr dmpPtr,
                 MotionPtr motion,
                 const std::string& nodeSetName,
                 float minTimestep=-1,
                 float maxTimestep=-1,
                 int nsamples=1000);

};

typedef boost::shared_ptr<MPLearner> MPLearnerPtr;

}

#endif
