#include "MPVisualizerDialog.h"
#include "ui_MPVisualizerDialog.h"
#include "../MPVisualizer.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/range/adaptor/transformed.hpp>
#include <QFileDialog>
#include <QMessageBox>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensorMeasurement.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <MMM/Motion/MotionWriterXML.h>

using namespace MMM;

MPVisualizerDialog::MPVisualizerDialog(MMM::MotionList motions, QWidget* parent) :
    QDialog(parent),
    ui(new Ui::MPVisualizerDialog),
    motions(motions)
{
    this->parent = parent;
    ui->setupUi(this);
    std::tuple<float, float> timestepTuple = MMM::Motion::calculateMinMaxTimesteps(motions);
    float minTimestep = std::get<0>(timestepTuple);
    float maxTimestep = std::get<1>(timestepTuple);

    ui->MinTimestepSpin->setMinimum(minTimestep);
    ui->MinTimestepSpin->setMaximum(maxTimestep);
    ui->MinTimestepSpin->setValue(minTimestep);

    ui->MaxTimestepSpin->setMinimum(minTimestep);
    ui->MaxTimestepSpin->setMaximum(maxTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);


    ui->GenMotionButton->setEnabled(false);
    ui->AddPointButton->setEnabled(false);
    ui->RemovePointButton->setEnabled(false);
    boost::filesystem::path p(motions[0]->getOriginFilePath());
    outfile = p.filename().stem().string();

    findHumanMotion();
    getAllAvailableNodeSets();
    ui->MotionNameEdit->setText(QString::fromStdString(humanMotion->getName() + "_vmp"));

    connect(ui->GenMotionButton, SIGNAL(clicked()), this, SLOT(generateMotion()));
    connect(ui->CancelButton, SIGNAL(clicked()), this, SLOT(close()));
    connect(ui->LoadMPButton, SIGNAL(clicked()), this, SLOT(loadMPFile()));
    connect(ui->LoadTempButton, SIGNAL(clicked()), this, SLOT(loadTempMotionFile()));

    connect(ui->LearnMPButton, SIGNAL(clicked()), this, SLOT(learnMP()));
    connect(ui->SaveMPButton, SIGNAL(clicked()), this, SLOT(saveMP()));

    connect(ui->AddPointButton, SIGNAL(clicked()), this, SLOT(addPoint()));
    connect(ui->RemovePointButton, SIGNAL(clicked()), this, SLOT(removePoint()));


    connect(ui->MinTimestepSpin, SIGNAL(valueChanged(double)), this, SLOT(minTimestepChanged(double)));
    connect(ui->HumanMotionCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(motionChanged(int)));

    connect(ui->GoalTableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(tableCellChanged(int,int)));
    connect(ui->GoalTableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(tableCellClicked(int,int)));

}

MPVisualizerDialog::~MPVisualizerDialog() {
    delete ui;
}

void MPVisualizerDialog::loadMPFile()
{
    std::string info0 = "Load MP Configuration File";
    std::string mpConfigFile = QFileDialog::getOpenFileName(parent, QString::fromStdString(info0), QDir::currentPath()).toStdString();

    if(mpConfigFile.empty())
        return;


    std::string info1 = "Load MP File";
    std::string mpFile = QFileDialog::getOpenFileName(parent, QString::fromStdString(info1),
                                      QString::fromStdString(mpConfigFile)).toStdString();


    if(mpFile.empty())
        return;


    ui->MPFileEdit->setText(QString::fromStdString(mpFile));
    ui->MPConfigEdit->setText(QString::fromStdString(mpConfigFile));

    mpVisualizer.reset(new MMM::MPVisualizer(mpFile, mpConfigFile));

    robot = VirtualRobot::RobotIO::loadRobot(mpVisualizer->getConfigPtr()->modelfilename);
    nodeSetName = mpVisualizer->getConfigPtr()->nodeSetName;
    fillGoalInTable();

    ui->GenMotionButton->setEnabled(true);
}

void MPVisualizerDialog::loadTempMotionFile()
{
    std::string info2 = "Load Template Motion File";
    std::string tempMotionFile = QFileDialog::getOpenFileName(parent, QString::fromStdString(info2),
                                                                QDir::currentPath()).toStdString();

    if(tempMotionFile.empty())
        return;

    ui->TempMotionEdit->setText(QString::fromStdString(tempMotionFile));
    mpVisualizer->setTempMotionFromFile(tempMotionFile);

    MMM::KinematicSensorPtr sensor = mpVisualizer->getTempMotion()->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);
    float minTimestep = sensor->getMinTimestep();
    float maxTimestep = sensor->getMaxTimestep();

    ui->MinTimestepSpin->setValue(minTimestep);
    ui->MaxTimestepSpin->setValue(maxTimestep);
}

void MPVisualizerDialog::generateMotion()
{
    if(!mpVisualizer->getTempMotion())
    {
        mpVisualizer->setTempMotion(humanMotion);
    }

    mpVisualizer->getConfigPtr()->goal = getRowVecFromTable(1, 1);

    std::map<double, DMP::DVec > viapoints;
    for(int i = 2; i < ui->GoalTableWidget->rowCount(); ++i)
    {
        double canVal = ui->GoalTableWidget->item(i, 0)->text().toDouble();
        DMP::DVec viapoint = getRowVecFromTable(i, 1);
        viapoints[canVal] = viapoint;
    }

    mpVisualizer->getConfigPtr()->viapoints = viapoints;

    float minTimestep = (float) ui->MinTimestepSpin->value();
    float maxTimestep = (float) ui->MaxTimestepSpin->value();

    MMM::MotionPtr newMotion = mpVisualizer->getMotion(1000, minTimestep, maxTimestep);
    newMotion->setName(ui->MotionNameEdit->text().toStdString());

    motions.push_back(newMotion);
    emit genMotion(motions);
}

void MPVisualizerDialog::fillGoalInTable()
{
    DMP::MPConfigPtr mpConfig = mpVisualizer->getConfigPtr();
    DMP::DVec goal = mpConfig->goal;
    std::string dmpType = mpConfig->dmpType;

    ui->GoalTableWidget->clear();
    int nrows = 2;

    if(dmpType == "UMITSMP")
    {
        int ncols = 8;

        ui->GoalTableWidget->setRowCount(nrows);
        ui->GoalTableWidget->setColumnCount(ncols);

        std::vector<std::string > names{"canVal", "x", "y", "z", "qw", "qx", "qy", "qz"};
        ui->GoalTableWidget->setItem(1, 0, new QTableWidgetItem(QString::number(0, 'f', 3)));

        for(int i = 0; i < ncols; ++i)
        {
            ui->GoalTableWidget->setItem(0, i, new QTableWidgetItem(QString::fromStdString(names[i])));
            if (i < (int) goal.size())
                ui->GoalTableWidget->setItem(1, i+1, new QTableWidgetItem(QString::number(goal[i], 'f', 3)));
        }

    }
    else
    {
        std::vector<std::string > nodeNames = robot->getRobotNodeSet(nodeSetName)->getNodeNames();
        ui->GoalTableWidget->setRowCount(nrows);
        ui->GoalTableWidget->setColumnCount(nodeNames.size()+1);
        ui->GoalTableWidget->setItem(0, 0, new QTableWidgetItem(QString::fromStdString("canVal")));
        ui->GoalTableWidget->setItem(1, 0, new QTableWidgetItem(QString::number(0, 'f', 3)));

        for(size_t i = 0; i < nodeNames.size(); ++i)
        {
            ui->GoalTableWidget->setItem(0, i+1, new QTableWidgetItem(QString::fromStdString(nodeNames[i])));
            ui->GoalTableWidget->setItem(1, i+1, new QTableWidgetItem(QString::number(goal[i], 'f', 3)));
        }
    }

    ui->AddPointButton->setEnabled(true);
}

DMP::DVec MPVisualizerDialog::getRowVecFromTable(int nrow, int jumpCols)
{
    int ncols = ui->GoalTableWidget->columnCount();
    if (nrow == -1)
        nrow = ui->GoalTableWidget->rowCount()-1;

    DMP::DVec goal;
    for(int i = jumpCols; i < ncols; i++)
    {
        double val = ui->GoalTableWidget->item(nrow,i)->text().toDouble();
        goal.push_back(val);
    }

    return goal;
}

void MPVisualizerDialog::getAllAvailableNodeSets()
{
    robot = VirtualRobot::RobotIO::loadRobot(humanMotion->getModel()->getFilename());
    std::vector<VirtualRobot::RobotNodeSetPtr > nodeSets = robot->getRobotNodeSets();

    for (size_t i = 0; i < nodeSets.size(); ++i)
    {
        VirtualRobot::RobotNodeSetPtr nodeSet = nodeSets[i];
        ui->NodeSetComboBox->addItem(QString::fromStdString(nodeSet->getName()));
    }
    nodeSetName = ui->NodeSetComboBox->currentText().toStdString();

}

void MPVisualizerDialog::addRowInTable(const DMP::DVec &point)
{
    ui->GoalTableWidget->insertRow(ui->GoalTableWidget->rowCount());
    for (size_t i = 0; i < point.size(); ++i)
    {
        ui->GoalTableWidget->setItem(ui->GoalTableWidget->rowCount()-1, i, new QTableWidgetItem(QString::number(point[i], 'f', 3)));
    }

    ui->RemovePointButton->setEnabled(true);
}

void MPVisualizerDialog::findHumanMotion()
{
    ui->HumanMotionCombo->clear();
    humanMotions.clear();
    for(size_t id = 0; id < motions.size(); ++id)
    {
        MMM::MotionPtr motion = motions[id];
        boost::filesystem::path modelFileName(motion->getModel()->getFilename());
        if (modelFileName.filename()=="mmm.xml")
        {
            MMM::KinematicSensorPtr sensor = motion->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);

            float minTimestep = sensor->getMinTimestep();
            float maxTimestep = sensor->getMaxTimestep();

            ui->MinTimestepSpin->setValue(minTimestep);
            ui->MaxTimestepSpin->setValue(maxTimestep);

            ui->HumanMotionCombo->addItem(QString::fromStdString(motion->getName()));
            humanMotions.push_back(motion);
        }
    }
    ui->HumanMotionCombo->setCurrentIndex(0);
    humanMotion = humanMotions[0];
}


void MPVisualizerDialog::minTimestepChanged(double val)
{
    double maxval = ui->MaxTimestepSpin->value();
    if (val > maxval)
        ui->MaxTimestepSpin->setValue(val+0.1);
}

void MPVisualizerDialog::learnMP()
{
    float minTimestep = ui->MinTimestepSpin->value();
    float maxTimestep = ui->MaxTimestepSpin->value();
    int nsamples = ui->NSamplesSpin->value();
    int kernelSize = ui->KernelSizeSpin->value();


    MMM::MPLearnerPtr mplearner(new MPLearner());

    std::string dmpType = "UMITSMP";
    if(ui->JointSpaceButton->isChecked())
    {
        dmpType = "UMIDMP";
    }

    DMP::DMPFactory factory;
    DMP::DMPInterfacePtr dmpPtr = factory.getDMP(dmpType, kernelSize);

    nodeSetName = ui->NodeSetComboBox->currentText().toStdString();
    ui->MotionNameEdit->setText(QString::fromStdString(humanMotion->getName() + "_" + nodeSetName + "_vmp"));

    DMP::MPConfigPtr mpConfig = mplearner->learnMP(dmpPtr, humanMotion, nodeSetName, minTimestep, maxTimestep, nsamples);

    mpVisualizer.reset(new MMM::MPVisualizer(dmpPtr, humanMotion, mpConfig));
    fillGoalInTable();
    ui->GenMotionButton->setEnabled(true);

}

void MPVisualizerDialog::motionChanged(int ind)
{
    humanMotion = humanMotions[ind];
    getAllAvailableNodeSets();
    ui->MotionNameEdit->setText(QString::fromStdString(humanMotion->getName() + "_" + nodeSetName + "_vmp"));
}

void MPVisualizerDialog::saveMP()
{
    std::string mpSaveDir = QFileDialog::getExistingDirectory(parent, QString::fromStdString("Select Save Directory"), QDir::currentPath()).toStdString();

    if (mpSaveDir.empty())
        mpSaveDir = QDir::currentPath().toStdString();

    std::string motionfile = mpSaveDir + "/" + outfile + "_" + humanMotion->getName() + "_tmp.xml";
    std::string outdmpfile = mpSaveDir + "/"+ outfile + "_" + humanMotion->getName() + "_" + nodeSetName + "_vmp.xml";
    std::string outdmpconfig = mpSaveDir + "/"+ outfile + "_" + humanMotion->getName() + "_" + nodeSetName + "_vmp_config.xml";

    mpVisualizer->getDMPPtr()->save(outdmpfile);
    mpVisualizer->getConfigPtr()->save(outdmpconfig);
    MMM::MotionWriterXMLPtr motionWriter(new MMM::MotionWriterXML());
    motionWriter->writeMotion(humanMotion, motionfile);
}

void MPVisualizerDialog::addPoint()
{

addRowInTable(getRowVecFromTable(-1,0));

}

void MPVisualizerDialog::removePoint()
{
    ui->GoalTableWidget->removeRow(ui->GoalTableWidget->rowCount()-1);
    if(ui->GoalTableWidget->rowCount() <= 2)
    {
        ui->RemovePointButton->setEnabled(false);
    }
}

void MPVisualizerDialog::tableCellChanged(int row, int col)
{
    if(col == 0 && row >= 2)
    {
        double canVal = ui->GoalTableWidget->item(row, col)->text().toDouble();
        if (canVal >= 0 && canVal <= 1)
        {
            ui->GoalTableWidget->item(row, col)->setBackgroundColor(QColor(255, 255, 255, 255));
        }
        else
        {
            ui->GoalTableWidget->item(row, col)->setBackgroundColor(QColor(255, 0, 0, 128));
        }
    }

}

void MPVisualizerDialog::tableCellClicked(int row, int col)
{
    if(col == 0 && row >= 2)
    {
        DMP::DVec targetState;
        double canVal = ui->GoalTableWidget->item(row, col)->text().toDouble();

        if (canVal >= 0 && canVal <= 1)
        {
            mpVisualizer->getDMPPtr()->prepareExecution(mpVisualizer->getConfigPtr()->goal, mpVisualizer->getConfigPtr()->initialState, 1.0, 1.0);
            mpVisualizer->getDMPPtr()->calculateDirectlyVelocity(mpVisualizer->getConfigPtr()->initialState,
                                                                 canVal,
                                                                 0.1,
                                                                 targetState);

            for (size_t i = 0; i < targetState.size(); ++i)
            {
                ui->GoalTableWidget->item(row, col+i+1)->setText(QString::number(targetState[i]));
            }
        }

    }
}


