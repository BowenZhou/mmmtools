#ifndef __MMM_MPVISUALIZERialog_H_
#define __MMM_MPVISUALIZERialog_H_

#include <QDialog>

#ifndef Q_MOC_RUN
#include <MMM/Motion/Motion.h>
#endif

#include "../MPVisualizer.h"
#include "../MPLearner.h"
#include <VirtualRobot/VirtualRobot.h>
namespace Ui {
class MPVisualizerDialog;
}

class MPVisualizerDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MPVisualizerDialog(MMM::MotionList motions, QWidget* parent = 0);
    ~MPVisualizerDialog();

private slots:
    void loadMPFile();
    void loadTempMotionFile();
    void generateMotion();
    void minTimestepChanged(double val);
    void learnMP();
    void motionChanged(int ind);
    void saveMP();
    void addPoint();
    void removePoint();
    void tableCellChanged(int row, int col);
    void tableCellClicked(int row, int col);


signals:
    void jumpTo(float timestep);
    void saveScreenshot(float timestep, const std::string &directory, const std::string &name = std::string());
    void genMotion(MMM::MotionList);

private:
    void fillGoalInTable();
    DMP::DVec getRowVecFromTable(int nrow, int jumpCols);
    void getAllAvailableNodeSets();
    void addRowInTable(const DMP::DVec& point);


    void findHumanMotion();

    Ui::MPVisualizerDialog* ui;
    MMM::MotionList motions;
    QWidget* parent;

    MMM::MPVisualizerPtr mpVisualizer;
    VirtualRobot::RobotPtr robot;
    std::string nodeSetName;
    MMM::MotionList humanMotions;
    MMM::MotionPtr humanMotion;
    std::string outfile;
};

#endif // __MMM_MPVisualizerDialog_H_
