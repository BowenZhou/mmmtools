#include "MPVisualizer.h"
#include <MMM/Motion/MotionWriterXML.h>
#include <MMM/Motion/Plugin/ModelPosePlugin/ModelPoseSensor.h>
#include <qdatetime.h>

MMM::MPVisualizer::MPVisualizer(const std::string &mpFile, const std::string &configFile)
{
    mpConfig.reset(new DMP::MPConfig(configFile));
    DMP::DMPFactory dmpfactory;
    dmpPtr = dmpfactory.getDMP(mpConfig->dmpType);
    dmpPtr->load(mpFile);
}

MMM::MPVisualizer::MPVisualizer(const std::string &mpFile, const std::string &configFile, const std::string &tempFile)
{
    setTempMotionFromFile(tempFile);
    mpConfig.reset(new DMP::MPConfig(configFile));

    DMP::DMPFactory dmpfactory;
    dmpPtr = dmpfactory.getDMP(mpConfig->dmpType);
    dmpPtr->load(mpFile);
}

MMM::MotionPtr MMM::MPVisualizer::getMotion(int nsamples, float minTimeStep, float maxTimeStep)
{

    /* create robot */
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(mpConfig->modelfilename);
    VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(mpConfig->nodeSetName);
    VirtualRobot::DifferentialIKPtr ik(new VirtualRobot::DifferentialIK(nodeSet,
                                                                      robot->getRootNode(),
                                                                      VirtualRobot::JacobiProvider::eSVDDamped));

    /* setup sensor */
//    MMM::KinematicSensorPtr tempSensor = tempMotion->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);

    MMM::KinematicSensorList allKineSensors = tempMotion->getSensorsByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);
    int kid = findKinematicSensorId(allKineSensors, nodeSet->getNodeNames());
    MMM::KinematicSensorPtr tempSensor = allKineSensors[kid];
    allKineSensors.erase(allKineSensors.begin() + kid);

    std::vector<std::string> jointNames = tempSensor->getJointNames();

    std::string motionName = tempMotion->getName() + "_" + mpConfig->nodeSetName + "_" + dmpPtr->getDMPType();
    MMM::MotionPtr motion(new MMM::Motion(motionName,
                                          tempMotion->getModel(),
                                          tempMotion->getModel(),
                                          tempMotion->getModelProcessor()));

    MMM::KinematicSensorPtr kineSensor(new MMM::KinematicSensor(jointNames));

    float t0 = tempSensor->getMinTimestep();
    float tT = tempSensor->getMaxTimestep();

    float nt0 = t0;
    float ntT = tT;
    if(minTimeStep >= 0)
        nt0 = minTimeStep;

    if(maxTimeStep > minTimeStep)
        ntT = maxTimeStep;

    DMP::Vec<DMP::DMPState> previousState = mpConfig->initialState;

    if (!mpConfig->viapoints.empty())
    {
        for(std::pair<double, DMP::DVec > viapoint : mpConfig->viapoints)
        {
            if(viapoint.first < 0 || viapoint.first > 1)
            {
                MMM_ERROR << "Cannot add viapoints with canval not in [0,1]" << std::endl;
                return NULL;
            }

            dmpPtr->setViaPoint(viapoint.first, viapoint.second);
        }
    }
    dmpPtr->prepareExecution(mpConfig->goal, mpConfig->initialState, 1.0, 1.0);

    for(int i = 0; i <= nsamples; ++i)
    {
        float ti = (float) i / (float) nsamples;
        float t = t0 + (tT - t0) * ti;
        float nt = nt0 + (ntT - nt0) * ti;
        double canVal = 1 + (-1) * (double) ti;

        MMM_INFO << "canVal: " << canVal << std::endl;
        DMP::DVec targetState;
        previousState = dmpPtr->calculateDirectlyVelocity(previousState, canVal, 1, targetState);

        MMM::KinematicSensorMeasurementPtr tempMeasure = tempSensor->getDerivedMeasurement(t);

        Eigen::VectorXf jvs = tempMeasure->getJointAngles();
        std::map< std::string, float > jointValueMap;
        if (mpConfig->dmpType == "UMITSMP")
        {
            for(int j = 0; j < jvs.rows(); ++j)
            {
                std::string jointName = jointNames[j];
                if(ik->getRobotNodeSet()->hasRobotNode(jointName))
                {
                    jointValueMap[jointName] = jvs[j];
                }
            }
            robot->setJointValues(jointValueMap);

            float qw = targetState[3];
            float qx = targetState[4];
            float qy = targetState[5];
            float qz = targetState[6];
            Eigen::Matrix4f goalPose = VirtualRobot::MathTools::quat2eigen4f(qx,qy,qz,qw);
            goalPose(0,3) = targetState[0];
            goalPose(1,3) = targetState[1];
            goalPose(2,3) = targetState[2];

            ik->setGoal(goalPose, nodeSet->getTCP());
            ik->solveIK();

            jointValueMap = nodeSet->getJointValueMap();
        }
        else
        {
            std::vector<std::string > jointNamesInNodeSet = nodeSet->getNodeNames();
            for(size_t j = 0; j < targetState.size(); ++j)
                jointValueMap[jointNamesInNodeSet[j]] = targetState[j];
        }

        Eigen::VectorXf jointAngles = jvs;
        for(int k = 0; k < jointAngles.rows(); ++k)
        {
            std::string jointName = jointNames[k];
            if(nodeSet->hasRobotNode(jointName))
            {
                jointAngles[k] = jointValueMap[jointName];
            }
        }

        MMM::KinematicSensorMeasurementPtr measure(new MMM::KinematicSensorMeasurement(nt, jointAngles));
        kineSensor->addSensorMeasurement(measure);
    }

    motion->addSensor(tempMotion->getSensorByType<MMM::ModelPoseSensor>(MMM::ModelPoseSensor::TYPE));
    motion->addSensor(kineSensor);
    MMM_INFO << allKineSensors.size() << std::endl;
    for(size_t i = 0; i < allKineSensors.size(); ++i)
    {
        MMM_INFO << allKineSensors[i]->getJointNames()[0] << std::endl;
        motion->addSensor(allKineSensors[i]);
    }

    return motion;
}

void MMM::MPVisualizer::generateAndExportMotion(int nsamples, float maxTimeStep, const std::string & outFile)
{
    MMM::MotionWriterXMLPtr motionWriter(new MMM::MotionWriterXML());
    MMM::MotionPtr motion = getMotion(nsamples, 0, maxTimeStep);
    motionWriter->writeMotion(motion, outFile);
}

void MMM::MPVisualizer::setTempMotionFromFile(const std::string &tempFile)
{
    MMM::MotionReaderXMLPtr motionReader(new MMM::MotionReaderXML());
    MMM::MotionList motions = motionReader->loadAllMotions(tempFile);
    tempMotion = motions[0];
}

int MMM::MPVisualizer::findKinematicSensorId(KinematicSensorList sensorList, const std::vector<std::string> &nodeSetNames)
{
    int id = -1;
    for(size_t i = 0; i < sensorList.size(); ++i)
    {
        MMM::KinematicSensorPtr kinesensor = sensorList[i];
        std::vector<std::string> jointNames = kinesensor->getJointNames();

        bool isfound = false;
        for(size_t j =0; j < nodeSetNames.size(); ++j)
        {
            if(std::count(jointNames.begin(), jointNames.end(), nodeSetNames[j]))
            {
                id=i;
                isfound = true;
            }
        }
        if(isfound) break;
    }

    if(id==-1)
    {
        MMM_ERROR << "Cannot find kinematic sensor corresponding to the nodeset. (Please check nodeset in the model file such as mmm.xml)" << std::endl;
        return id;
    }

    return id;
}


