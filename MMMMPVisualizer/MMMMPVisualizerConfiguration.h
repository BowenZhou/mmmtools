#ifndef __MMM_MMMMPVISUALIZERCONFIGURATION_H_
#define __MMM_MMMMPVISUALIZERCONFIGURATION_H_

#include <filesystem>
#include <string>

#include <MMM/MMMCore.h>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>

#include <VirtualRobot/RuntimeEnvironment.h>

#include "../common/ApplicationBaseConfiguration.h"

/*!
    Configuration of MPPlugin.
*/
class MMMMPVisualizerConfiguration : public ApplicationBaseConfiguration
{

public:
    std::string tempMotionFile;
    std::string mpFile;
    std::string mpConfigFile;
    std::string outputFileName;
    int numSamples;
    float timeDuration;

    MMMMPVisualizerConfiguration() : ApplicationBaseConfiguration()
    {
    }

    bool processCommandLine(int argc, char *argv[])
    {
        VirtualRobot::RuntimeEnvironment::considerKey("tempMotionFile", "mmm template motion file path");
        VirtualRobot::RuntimeEnvironment::considerKey("mpFile", "mp file");
        VirtualRobot::RuntimeEnvironment::considerKey("mpConfigFile", "mp config file");
        VirtualRobot::RuntimeEnvironment::considerKey("outputFileName", "output file name");
        VirtualRobot::RuntimeEnvironment::considerKey("numSamples", "number of samples");
        VirtualRobot::RuntimeEnvironment::considerKey("TimeDuration", "motion time duration");

        VirtualRobot::RuntimeEnvironment::processCommandLine(argc,argv);
        VirtualRobot::RuntimeEnvironment::print();

        tempMotionFile = getParameter("tempMotionFile", true, true);
        mpFile = getParameter("mpFile", true, true);
        mpConfigFile = getParameter("mpConfigFile", true, true);

        outputFileName = getParameter("outputDir", false, true);
        if (outputFileName.empty())
            outputFileName = std::filesystem::path(mpFile).string() + "_motion.xml";

        numSamples = VirtualRobot::RuntimeEnvironment::getValue<int>("numSamples", 1000);
        timeDuration = VirtualRobot::RuntimeEnvironment::getValue<float>("TimeDuration", 10);

        return valid;
    }

    void print()
    {
        MMM_INFO << "*** MMMMPVisualizer Configuration ***" << std::endl;
        std::cout << "Template motion: " << tempMotionFile << std::endl;
        std::cout << "MP file: " << mpFile << std::endl;
        std::cout << "MP Configuration file: " << mpConfigFile << std::endl;
    }
};


#endif
