#include "MPLearner.h"

DMP::MPConfigPtr MMM::MPLearner::learnMP(DMP::DMPInterfacePtr dmpPtr,
                             MMM::MotionPtr motion,
                             const std::string &nodeSetName,
                             float minTimestep,
                             float maxTimestep,
                             int nsamples)
{
    std::string modefileName = motion->getModel()->getFilename();
    VirtualRobot::RobotPtr robot = VirtualRobot::RobotIO::loadRobot(modefileName);
    VirtualRobot::RobotNodeSetPtr nodeSet = robot->getRobotNodeSet(nodeSetName);
    std::map< std::string, float > jointValueMap = nodeSet->getJointValueMap();

    MMM::KinematicSensorPtr sensor = motion->getSensorByType<MMM::KinematicSensor >(MMM::KinematicSensor::TYPE);
    std::vector<std::string> jointNames = sensor->getJointNames();

    float t0 = sensor->getMinTimestep();
    if (minTimestep >= t0)
        t0 = minTimestep;

    float tT = sensor->getMaxTimestep();
    if (maxTimestep > t0 && maxTimestep < tT)
        tT = maxTimestep;


    std::map<double, DMP::DVec> tcpTrajData;
    std::map<double, DMP::DVec> jointTrajData;

    VirtualRobot::MathTools::Quaternion oldq;
    for (int i = 0; i <= nsamples; i++)
    {
        float t = t0 + (tT - t0) * (float) i / (float) nsamples;

        MMM::KinematicSensorMeasurementPtr measurment = sensor->getDerivedMeasurement(t);
        Eigen::VectorXf jvs = measurment->getJointAngles();
        for(int j = 0; j < jvs.rows(); ++j)
        {
            std::string jointName = jointNames[j];
            if(nodeSet->hasRobotNode(jointName))
            {
                jointValueMap[jointName] = jvs[j];
            }
        }


        robot->setJointValues(jointValueMap);

        Eigen::Matrix4f tcpPose = nodeSet->getTCP()->getPoseInRootFrame();
        VirtualRobot::MathTools::Quaternion quat = VirtualRobot::MathTools::eigen4f2quat(tcpPose);

        if (i==0)
            oldq = quat;
        else
        {
            float cosHalfTheta = oldq.w * quat.w + oldq.x * quat.x + oldq.y * quat.y + oldq.z * quat.z;
            if(cosHalfTheta < 0)
            {
                quat.w = -quat.w;
                quat.x = -quat.x;
                quat.y = -quat.y;
                quat.z = -quat.z;
            }
            oldq = quat;
        }

        DMP::DVec data;
        for(int j = 0; j < 3; ++j)
            data.push_back(tcpPose(j, 3));

        data.push_back(quat.w);
        data.push_back(quat.x);
        data.push_back(quat.y);
        data.push_back(quat.z);
        double ti = (double) i / (double) nsamples;
        tcpTrajData[ti] = data;


        DMP::DVec jdata;
        std::vector<float> jointvals = nodeSet->getJointValues();
        for(size_t j = 0; j < jointvals.size(); ++j)
            jdata.push_back((double) jointvals[j]);

        jointTrajData[ti] = jdata;
    }

    DMP::SampledTrajectoryV2 traj;

    if (dmpPtr->getDMPType() == "UMITSMP")
    {
        traj = DMP::SampledTrajectoryV2(tcpTrajData);
    }
    else
    {
        traj = DMP::SampledTrajectoryV2(jointTrajData);
    }

    DMP::Vec<DMP::SampledTrajectoryV2 > trajs;
    trajs.push_back(traj);

    dmpPtr->learnFromTrajectories(trajs);

    DMP::MPConfigPtr mpConfig(new DMP::MPConfig(dmpPtr));
    mpConfig->motionName = motion->getName();
    mpConfig->nodeSetName = nodeSetName;
    mpConfig->modelfilename = modefileName;
    mpConfig->timeDuration = tT - t0;
    return mpConfig;
}
