#ifndef __MMM_MPVISUALIZER_H_
#define __MMM_MPVISUALIZER_H_

#include <MMM/Motion/MotionReaderXML.h>
#include <dmp/representation/dmpfactory.h>
#include <dmp/representation/dmpconfig.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/MathTools.h>
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <VirtualRobot/IK/DifferentialIK.h>
#include <MMM/Motion/Plugin/KinematicPlugin/KinematicSensor.h>

#include <filesystem>

namespace MMM {

class MPVisualizer
{
public:
    MPVisualizer(const std::string& mpFile, const std::string& configFile);
    MPVisualizer(const std::string& mpFile, const std::string& configFile, const std::string& tempFile);
    MPVisualizer(DMP::DMPInterfacePtr dmpPtr, MotionPtr tempMotion, DMP::MPConfigPtr mpConfig):
        dmpPtr(dmpPtr), tempMotion(tempMotion), mpConfig(mpConfig)
    {}

    MotionPtr getMotion(int nsamples, float minTimeStep = -1, float maxTimeStep = -1);

    void generateAndExportMotion(int nsamples, float maxTimeStep, const std::string& outFile);

    DMP::MPConfigPtr getConfigPtr(){return mpConfig;}
    DMP::DMPInterfacePtr getDMPPtr(){return dmpPtr;}
    MotionPtr getTempMotion(){return tempMotion;}
    void setTempMotion(MotionPtr motion){tempMotion=motion;}
    void setTempMotionFromFile(const std::string& tempFile);

private:
    DMP::DMPInterfacePtr dmpPtr;
    MotionPtr tempMotion;
    DMP::MPConfigPtr mpConfig;

    int findKinematicSensorId(KinematicSensorList sensorList, const std::vector<std::string>& nodeSetNames);
};

typedef boost::shared_ptr<MPVisualizer> MPVisualizerPtr;

}

#endif // __MMM_C3DCONVERTER_H_
